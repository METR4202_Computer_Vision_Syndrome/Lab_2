clear all
close all
%% Calibration code

%% define image source
cam = webcam;
cam.Resolution = '640x480';
cam.Brightness = 50;
preview(cam);
pause(10);
%% acquire calibration images
delete('calibrationimages/*');
numberOfViews = 0;
numberToTake = 10;
fprintf('Give me some calibration images, need to take %d images\n', numberToTake);
for num = 1:numberToTake;
 fprintf('Views needed: %d, you have 5s\n', numberToTake-num+1);
 pause(5);
 img = snapshot(cam);
 fname = ['calibrationimages/Image' num2str(num) '.png'];
 imwrite(img,fname,'png');
 numberOfViews = numberOfViews + 1;
end

fprintf('Take image with flat checkerboard, you have 10s\n');
pause(10);
im_horiz = snapshot(cam);
fname = 'horiz.jpg';
imwrite(im_horiz,fname);

fprintf('Take image with vertical checkerboard, you have 10s\n');
pause(10);
im_vert = snapshot(cam);
fname = 'vert.jpg';
imwrite(im_vert,fname);
fprintf('Finished, may need to retake bad images, standby\n');
closePreview(cam);
%% Calibration do
images = imageSet(fullfile(pwd,'calibrationimages'));
imageFileNames = images.ImageLocation;

[imagePoints,boardSize,imagesUsed1] = detectCheckerboardPoints(imageFileNames);
squareSize = 24; % millimeters
worldPoints = generateCheckerboardPoints(boardSize, squareSize);                                                                                                                                                                                

[CameraParams,imagesUsed2,estimationErrors] = estimateCameraParameters(imagePoints, worldPoints);

%% check if all 10 weren't good //get additional images once
numberToRetake = numberToTake - (sum(imagesUsed1));

if (numberToRetake > 0)
 fprintf('Some images were bad, need to retake %d images\n', numberToRetake);
 preview(cam);
 pause(10);
for num = 1:numberToRetake;
 fprintf('Views needed: %d, you have 5s\n', numberToRetake-num+1);
 pause(5);
 img = snapshot(cam);
 fname = ['calibrationimages/Image' num2str(numberOfViews+1) '.png'];
 imwrite(img,fname);
 numberOfViews = numberOfViews + 1;
end
closePreview(cam);
% rerun calib
images = imageSet(fullfile(pwd,'calibrationimages'));
imageFileNames = images.ImageLocation;

[imagePoints, boardSize] = detectCheckerboardPoints(imageFileNames);
squareSize = 24; % millimeters
worldPoints = generateCheckerboardPoints(boardSize, squareSize);                                                                                                                                                                                

[CameraParams,imagesUsed,estimationErrors] = estimateCameraParameters(imagePoints, worldPoints);
displayErrors(estimationErrors, CameraParams);
else
 fprintf('All 10 images were good. Standby\n');
end

%% Print a graph 
showReprojectionErrors(CameraParams);
figure;
showExtrinsics(CameraParams);
drawnow;

% Calibration Over
%Redundant
%imOrig = imread('test6.png');


% Prepare Calibration Images
% Create a cell array of file names of calibration images.
numImages = 10;
files = cell(1, numImages);
for i = 1:numImages
 files{i} = fullfile('C:\Users\Meet\OneDrive\Documents\Third Year\SEM2\METR4202\plswrk2 (1)\plswrk', 'images6', sprintf('image%d.jpg', i));
end


%% Estimate Camera Parameters

% Detect the checkerboard corners in the images.
[imagePoints, boardSize] = detectCheckerboardPoints(files);


% Generate the world coordinates of the checkerboard corners in the
% pattern-centric coordinate system, with the upper-left corner at (0,0).
squareSize = 24; % in millimeters
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibrate the camera.
CameraParams = estimateCameraParameters(imagePoints, worldPoints);

% Get a scene and undistort it

fprintf('Remove calibration checkerboard, implement central frame and dominoes, 60s\n');
% pause(60);

imOrig = snapshot(cam);
im = undistortImage(imOrig,CameraParams);
[imagePoints2, boardSize2] = detectCheckerboardPoints(im);
imagePoints2 = imagePoints2';
imagePoints2 = [imagePoints2(2,:); imagePoints2(1,:)]';

% point1 = [imagePoints2(1,1) imagePoints2(1,2)];
% point2 = [imagePoints2(end,1) imagePoints2(end,2)];
%%
figure(1)
imshow(im);
hold on
%plot([point1(2) point2(2)],[point1(1) point2(1)],'Linewidth',2,'Color','red')
title('Orig');
%radii = [20];
%viscircles(point1, radii,'EdgeColor','b');
%viscircles(point2, radii,'EdgeColor','g');
hold off

squareSize2 = 11.0;
worldPoints2 = generateCheckerboardPoints(boardSize2,squareSize2);

worldPoints2= worldPoints2';
worldPoints2 = [worldPoints2(2,:); worldPoints2(1,:)]';

[rotationMatrix, translationVector] = extrinsics(imagePoints2, worldPoints2, CameraParams);

point1 = [worldPoints2(1,1) worldPoints2(1,2)];
point2 = [worldPoints2(end,1) worldPoints2(end,2)];

figure(2)
hold on
plot([point1(1) point2(1)],[point1(2) point2(2)],'Linewidth',2,'Color','red')
radii = [20];
viscircles(point1, radii,'EdgeColor','b');
viscircles(point2, radii,'EdgeColor','g');
title('lineabstract');
hold off

figure(3);
plot3(worldPoints2(:,1),worldPoints2(:,2),zeros(size(worldPoints2, 1),1),'*');
hold on
plot3(worldPoints2(1,1),worldPoints2(1,2),0,'g*');
orientation = rotationMatrix';
location = -translationVector * orientation;
location = [location(1) location(2) location(3)-165];
distance = norm(location)
cam = plotCamera('Location',location,'Orientation',orientation,'Size',20);
set(gca,'CameraUpVector',[0 0 -1]);
camorbit(gca,-110,60,'data',[0 0 1]);
axis equal
grid on
xlabel('X (mm)');
ylabel('Y (mm)');
zlabel('Z (mm)');




% Distance to frame done


%% Distance between dominos
posframe = [imagePoints2(1,1) imagePoints2(1,2)];
posdomArray = centroids;
worldPointFrame = pointsToWorld(CameraParams, rotationMatrix, translationVector, posframe);

distances = [];
angles = [];
xArray = [];
yArray = [];

for idx = 1:numel(posdomArray)/2;
    posx = posdomArray(idx,1);
    posy = posdomArray(idx,2);
    posdom = [posx posy];
    worldPointDom = pointsToWorld(CameraParams, rotationMatrix, translationVector, posdom);
    xArray = [xArray, worldPointDom(1)];
    yArray = [yArray, worldPointDom(2)];
    
    distancePoint1 = [abs(worldPointDom); abs(worldPointFrame)];
    distance1 = pdist(distancePoint1, 'euclidean');
    distances = [distances, distance1];
    costheta = dot(worldPointDom,worldPointFrame)/(norm(worldPointDom)*norm(worldPointFrame));
    if worldPointDom(2) > worldPointFrame(2); 
        if worldPointDom(2) > worldPointFrame(2);
            theta = radtodeg(acos(costheta));
        else 
            theta = 180 - radtodeg(acos(costheta));
        end
    else
        if worldPointDom(1) > worldPointFrame(1);
            theta = -radtodeg(x1xacos(costheta));
        else 
            theta = -180 + radtodeg(acos(costheta));
        end
    end
    angles = [angles, theta];
end
distances
angles

