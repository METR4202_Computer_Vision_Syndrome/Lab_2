Ryan coded the logic for determining if a domino is present and if so, its value, based on feature detection. 
Ryan also helped out with sensor placement (detecting the checkerboard reference frame). 

Jesse experimented with various feature detection techniques such as Hough lines and circles and Harris corners. and attempted sensor placement with fiducials. In the end, blob detection was used on binary images. 

Sergiy wrote code to find the distance between detected dominos and the reference frame. 
Sergiy also helped out with feature detection. 

Meetkumar worked on recovering sensor placement by detecting the checkerboard reference frame and recovering its pose via the rotation and translation matrix. 
Along with Andy, Meetkumar also worked on calibration with a large checkerboard and obtaining the camera intrinsics. 

Andy worked on various video tracking techniques and ended up using foreground and blob detection. Andy also worked with Meetkumar on calibration. 
