function [rectx,recty,x_bar,y_bar] = drawtrace(img, B, L, N, A);
doDraw = 0;
figure; imshow(img); hold on;
% Loop through object boundaries
rectx = 'nah';
recty = 'nah';
for k = 1:1
    % Boundary k is the parent of a hole if the k-th column
    % of the adjacency matrix A contains a non-zero element
    if (nnz(A(:,k)) > 0)
        boundary = B{k};
        plot(boundary(:,2),...
            boundary(:,1),'r','LineWidth',2);
        [rectx,recty,area,perimeter] = minboundrect(xv,yv,'a');
        plot(rectx,...
            recty,'r','LineWidth',2);
        rectangleArray = [];
        rectanglesCoord = [];
            for i=1:4
                rectangleArray = [rectangleArray rectx(i) recty(i)];
            end
            rectanglesCoord = [rectanglesCoord; rectangleArray];
			
			% split the domino into two so we can get the values of each half
            doDraw = 1;
            distanceOne = (rectangleArray(3)-rectangleArray(1))^2 + (rectangleArray(4)-rectangleArray(2))^2;
            distanceTwo = (rectangleArray(5)-rectangleArray(3))^2 + (rectangleArray(6)-rectangleArray(4))^2;
            if distanceOne < distanceTwo
                vectorShort = [rectangleArray(3) rectangleArray(4)] - [rectangleArray(1) rectangleArray(2)];
                vectorLong = [rectangleArray(5) rectangleArray(6)] - [rectangleArray(3) rectangleArray(4)];
                pointOne = [(rectangleArray(3)+rectangleArray(5))/2 (rectangleArray(4)+rectangleArray(6))/2];
                pointTwo = [(rectangleArray(1)+rectangleArray(7))/2 (rectangleArray(2)+rectangleArray(8))/2];
                xPlot = [rectangleArray(1) rectangleArray(3) pointOne(1) pointTwo(1) rectangleArray(1)];
                yPlot = [rectangleArray(2) rectangleArray(4) pointOne(2) pointTwo(2) rectangleArray(2)];
                xPlot2 = [pointOne(1) pointTwo(1) rectangleArray(7) rectangleArray(5) pointOne(1)];
                yPlot2 = [pointOne(2) pointTwo(2) rectangleArray(8) rectangleArray(6) pointOne(2)];

            else
                vectorShort = [rectangleArray(5) rectangleArray(6)] - [rectangleArray(3) rectangleArray(4)];
                vectorLong = [rectangleArray(3) rectangleArray(4)] - [rectangleArray(1) rectangleArray(2)];
                pointOne = [(rectangleArray(7)+rectangleArray(5))/2 (rectangleArray(8)+rectangleArray(6))/2];
                pointTwo = [(rectangleArray(1)+rectangleArray(3))/2 (rectangleArray(2)+rectangleArray(4))/2];
                xPlot = [rectangleArray(5) rectangleArray(3) pointTwo(1) pointOne(1) rectangleArray(5)];
                yPlot = [rectangleArray(6) rectangleArray(4) pointTwo(2) pointOne(2) rectangleArray(6)];
                xPlot2 = [pointTwo(1) pointOne(1) rectangleArray(7) rectangleArray(1) pointTwo(1)];
                yPlot2 = [pointTwo(2) pointOne(2) rectangleArray(8) rectangleArray(2) pointTwo(2)];
            end
            plot(xPlot2,yPlot2,'g','LineWidth',2)
            plot(xPlot,yPlot,'b','LineWidth',2)
        

        
        dominoValue1 = 0;
        dominoValue2 = 0;
    end
end



        CC2 = bwconncomp(imcomplement(img));
        no_obj2 = CC2.NumObjects;
        stats2 = regionprops(CC2, 'Area', 'BoundingBox');
        % the circles end up having a very small area
        idx2=find(([stats2.Area] > 2));% & ([stats2.Area] < 6000)
        dominoValue1 = 0;
        dominoValue2 = 0;
        % find the blob with the max area - this is probably the
        % rectangular line that is in the middle of the domino
        maxArea = 0;
        for j=1:length(idx2)
            bb2 = stats2(idx2(j)).BoundingBox;
            area = stats2(idx2(j)).Area;
            if area<8000 && area>maxArea
                maxArea = area;
            end
        end
        % try to count the values of each half of the domino
        for j=1:length(idx2)
            bb2 = stats2(idx2(j)).BoundingBox;
            area = stats2(idx2(j)).Area;
            if area<8000 && area~=maxArea %(maxArea/area>1.75 && maxArea/area<2.25)
%                set(0,'CurrentFigure',h);
%                rectangle('Position', bb2, 'EdgeColor', 'red');
                % bb2(1) = x bb(3) = x dir length
                x_middle = bb2(1) + bb2(3)/2
                y_middle = bb2(2) + bb2(4)/2
                in1 = inpolygon(x_middle, y_middle, xPlot, yPlot);
                in2 = inpolygon(x_middle, y_middle, xPlot2, yPlot2);
                if numel(in1) > 0
                    dominoValue1 = dominoValue1 + 1;
                elseif numel(in2) > 0
                    dominoValue2 = dominoValue2 + 1;
                end
            end
        end





% display the value of each half of the domino
if doDraw
            if dominoValue1>6
                dominoValue1 = 6
            end
            if dominoValue2>6
                dominoValue2 = 6
            end  
            xvCol = xPlot;
            yvCol = yPlot;
            A = xvCol(1:end-1).*yvCol(2:end)-xvCol(2:end).*yvCol(1:end-1);
            As = sum(A)/2;
            x_bar1 = (sum((xvCol(2:end)+xvCol(1:end-1)).*A)*1/6)/As;
            y_bar1 = (sum((yvCol(2:end)+yvCol(1:end-1)).*A)*1/6)/As;

            xvCol = xPlot2;
            yvCol = yPlot2;
            A = xvCol(1:end-1).*yvCol(2:end)-xvCol(2:end).*yvCol(1:end-1);
            As = sum(A)/2;
            x_bar2 = (sum((xvCol(2:end)+xvCol(1:end-1)).*A)*1/6)/As;
            y_bar2 = (sum((yvCol(2:end)+yvCol(1:end-1)).*A)*1/6)/As;
            
            text(x_bar1, y_bar1, num2str(dominoValue1),'FontSize', 20, 'Color', 'red', 'HorizontalAlignment', 'center');
            text(x_bar2, y_bar2, num2str(dominoValue2),'FontSize', 20, 'Color', 'red', 'HorizontalAlignment', 'center');
end

    end
