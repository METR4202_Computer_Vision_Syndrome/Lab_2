%% Setup
close all; clear all; 
%% Video characteristics
fps = 30; %fps
speedscale = 30/16;
% frame interval:
t = speedscale * 1/fps;

%% Record video
clear cam 
cam = webcam(2);
cam.Brightness = 140;
%cam.Resolution = '1280x720';
preview(cam);
v = VideoWriter('video.avi');
open(v)
frames = 400;
recordtime = frames*t;
fprintf('recording video for %i seconds', recordtime)
for iFrame = 1:frames                    % Capture 100 frames
   writeVideo(v,snapshot(cam));
end
close(v)
fprintf('finished recording video')
clear cam

%% Import Video, Initialize Foreground Detector
foregroundDetector = vision.ForegroundDetector('NumGaussians', 2, ...
    'NumTrainingFrames', 50, 'MinimumBackgroundRatio', 0.92);

videoReader = vision.VideoFileReader('video.avi');
for i = 1:150
    frame = step(videoReader); % read the next video frame
    foreground = step(foregroundDetector, frame);
end

%figure; imshow(frame); title('Video Frame');
%figure; imshow(foreground); title('Foreground');
    
%% Detect domino's in an initial frame (optional)

%se = strel('square', 3);
%filteredForeground = imopen(foreground, se);
%figure; imshow(filteredForeground); title('Clean Foreground');

%% Setup Blob Analysis
 blobAnalysis = vision.BlobAnalysis('BoundingBoxOutputPort', true, ...
    'AreaOutputPort', false, 'CentroidOutputPort', true, ...
    'MinimumBlobArea', 150, 'LabelMatrixOutputPort', true); %'MaximumBlobArea', 1000, 
%% Process the video
% In the final step, we process the remaining video frames.
videoPlayer = vision.VideoPlayer('Name', 'Detected Domino''s');
videoPlayer.Position(3:4) = [650,400];  % window size: [width, height]
se = strel('square', 3); % morphological filter for noise removal
centroidold = [0 0];
vp = 0;
centroid = [0 0];
maxsize = 150;
minsize = 30;
    
while ~isDone(videoReader)
    
    frame = step(videoReader); % read the next video frame
    
    % Detect the foreground in the current video frame
    foreground = step(foregroundDetector, frame);
    
    % Use morphological opening to remove noise in the foreground
    filteredForeground = imopen(foreground, se);
    
    % Detect the connected components with the specified minimum area, and
    % compute their bounding boxes
   
    [centroid, bbox] = step(blobAnalysis, filteredForeground);
    
    dominos = [];
    locations = [];
    [m,n] = size(bbox);
    for i = 1:m
        if (bbox(i,3)<maxsize && bbox(i,3)>minsize && bbox(i,4)<maxsize && bbox(i,4)>minsize)
            dominos = [dominos; bbox(i,:)];
            locations = [locations; centroid(i,:)];
        end
    end
    bbox = dominos;
    centroid = locations;
     
    % Draw bounding boxes around the detected cars
    result = insertShape(frame, 'Rectangle', bbox, 'Color', 'green');

    % Display the number of cars found in the video frame
    numCars = size(bbox, 1);
    result = insertText(result, [10 10], numCars, 'BoxOpacity', 1, ...
        'FontSize', 14);
    
    [m,n] = size(centroid);
    [o,p] = size(centroidold);
    %if (m==o && n==p)
    for j = 1:m
        if j <= o && m==o
            d = sqrt((centroid(j, 1)-centroidold(j, 1))^2 +  (centroid(j, 2)-centroidold(j, 2))^2);
            if d < 50
               %if m==o && n==p
                    vp = round(d/t); %pixels/second
                    scale = bbox(j,3);
                    v = d*6/5/1000/t;
                    result = insertText(result, [centroid(j, 1) centroid(j, 2)],['pixel speed: ' num2str(vp) ' pix/s']);
                    result = insertText(result, [centroid(j, 1) centroid(j, 2)+20],['2D speed: ' num2str(v) ' m/s']);
                
            end
        else
            if j<=o
                result = insertText(result, [centroidold(j, 1) centroidold(j, 2)],['pixel speed:          ']);
                result = insertText(result, [centroidold(j, 1) centroidold(j, 2)+20],['2D speed:                   ']);
            end
        end
    end
   
    centroidold = centroid;

   

    step(videoPlayer, result);  % display the results
end

release(videoReader); % close the video file
close all;