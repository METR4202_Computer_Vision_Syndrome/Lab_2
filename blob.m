close all
clear cam

horizontal = 0;
advanced = 0;

cam = webcam(2);
cam.Resolution = '1920x1080';
cam.Brightness = 50;
pause(10);
% take picture of workspace
BW = snapshot(cam);
BWORIGINAL = BW;
%%%%%figure;imshow(BW)
% crop the picture of the workspace
BW=imcrop(BW,[0 300 1920-200 1080-300]);
%%%%%figure;imshow(BW)
BWBackup = BW;

% filter out the blue channel
B = BW(:,:,3);
figure;imshow(B);
t = 0.4; %graythresh(B); 
x = 50;
% run the Bolshevik function (approximately sounds like BS)
[idx1,stats1,rectanglesCoord,centroids] = Bolshevik(t,x,B);

h2=figure;%imshow(BW);

function [idx,stats,rectanglesCoord,centroids] = Bolshevik(t,x,B)
    rectanglesCoord = [];
    % make the image binary
    BW2 =imbinarize(B, t);
    %%%%%figure;imshow(BW2)
    % [BB,L,N,A] = bwboundaries(BW2);
    % drawtrace(BW2,BB,L,N,A);

    BW3 = bwareaopen(BW2, x);
    BW4 = bwmorph(BW3, 'clean');
    %BW5 = bwmorph(BW4, 'open');
    h=figure;%imshow(BW4)
    %Bound = bwboundaries(BW2);
    
	% run blob detection
    CC = bwconncomp(BW4); %BW4
    no_obj = CC.NumObjects;
    stats = regionprops(CC, 'Area', 'BoundingBox');
    idx=find((4000 < [stats.Area]) & ([stats.Area] < 17000));

    dominoValues = [];
    numDominos = 0;
    centroids = [];
    for i=1:length(idx)
        bb = stats(idx(i)).BoundingBox;
        %set(0,'CurrentFigure',h);
        %rectangle('Position', bb, 'EdgeColor', 'red');
        % crop the image according to the blob detection area so we can 
        % focus on the prospective domino's features
        BW_dom_cropped = imcrop(BW4,[bb(1)-10 bb(2)-10 bb(3)+20 bb(4)+20]);
        BWforTrace = BW_dom_cropped;
        BW_dom_cropped = imcomplement(BW_dom_cropped);
        figure;imshow(BW_dom_cropped);
        
        % trace the outline of the domino inside the cropped image
        [BB,L,N,A] = bwboundaries(BWforTrace);
        [rectx,recty,xbar,ybar] = drawtrace(BWforTrace,BB,L,N,A);
        % get the centroids of the domino to use later to find the
        % distance to the reference frame
        centroids = [centroids; xbar ybar];
end
